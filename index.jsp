<%-- 
    Document   : index
    Created on : Mar 1, 2018, 12:38:40 PM
    Author     : SanthoshMmanikandan
--%>



<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Transaction"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<html>
    <head>
        <title>Registration Form</title>
        <style type="text/css">
            h3{font-family: Calibri; font-size: 22pt; font-style: normal; font-weight: bold; color:#64c4b4;
               text-align: center; text-decoration: underline }
            table{font-family: Calibri; color:white; font-size: 11pt; font-style: normal;width: 50%;
                  text-align:; background-color: #64c4b4; border-collapse: collapse; border: 2px solid navy}
            table.inner{border: 0px}
        </style>


        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script>
            $(function () {
                $("#datepicker").datepicker();
            });
        </script>

    </head>
    <body>
        
         <%
        
        
        
         Configuration cf = new Configuration();
            cf.configure("hibernate.cfg.xml");
            SessionFactory sf = cf.buildSessionFactory();
            Session s = sf.openSession();
            Transaction t = s.beginTransaction();
            Query q=s.createQuery("select max(Patientid) from PatientRegisterRecord");
            List l=q.list();
            System.out.println("Max Id"+l.get(0));
            String ss=(String)l.get(0);
            
        int id=Integer.parseInt(ss)+1;
        String Patientid="SDC"+id;
        t.commit();
        s.close();
        sf.close();
        
        
        %>
    
        <br>       
        <h3>Patient Registration Form</h3>
        <form action="NewServlet" method="POST">
            <table align="center" cellpadding = "10">
                <tr>
                    <td>Patient ID</td>
                <input type="text" value="<%=ss%>" name="Tempid">
                    <td><input type="text" name="Patientid" maxlength="30" value="<%=Patientid%>" required/>

                   
                    </td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td><input type="date"  name="Date1" id="datepicker" required/>

                    </td>
                </tr>
                <tr>
                    <td>First Name *</td>
                    <td><input type="text" name="Patientfirstname" maxlength="30"  required/>

                    </td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td><input type="text" name="Patientlastname" maxlength="30"   required />

                    </td>
                </tr>

                <tr>
                    <td>Age *</td>
                    <td><select name="Patientage"><option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option><option value="61">61</option><option value="62">62</option><option value="63">63</option><option value="64">64</option><option value="65">65</option><option value="66">66</option><option value="67">67</option><option value="68">68</option><option value="69">69</option><option value="70">70</option><option value="71">71</option><option value="72">72</option><option value="73">73</option><option value="74">74</option><option value="75">75</option><option value="76">76</option><option value="77">77</option><option value="78">78</option><option value="79">79</option><option value="80">80</option><option value="81">81</option><option value="82">82</option><option value="83">83</option><option value="84">84</option><option value="85">85</option><option value="86">86</option><option value="87">87</option><option value="88">88</option><option value="89">89</option><option value="90">90</option><option value="91">91</option><option value="92">92</option><option value="93">93</option><option value="94">94</option><option value="95">95</option><option value="96">96</option><option value="97">97</option><option value="98">98</option><option value="99">99</option><option value="100">100</option></select>
                    </td>
                </tr> 

                <tr>
                    <td>Sex *</td>
                    <td> 

                        <select name="Patientsex">
                            <option value="None selected">Please select below</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Other">Other</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Blood Group *</td>
                    <td>
                        <select name="Patientbloodgroup">
                            <option value="None selected">Please select below</option>
                            <option value="A+">A+</option>
                            <option value="A-">A-</option>
                            <option value="AB+">AB+</option>
                            <option value="AB-">AB-</option>
                            <option value="B+">B+</option>
                            <option value="B-">B-</option>
                            <option value="o+">O+</option>
                            <option value="O-">O-</option>
                        </select></td>
                </tr>
                <tr>
                    <td>Mobile Number *</td>
                    <td><input type="text" name="PatientMobilenumber" maxlength="100" required/></td>
                </tr>
                <tr>
                    <td>Marital Status</td>
                    <td>
                        <select name="Patientmaritalstatus">
                            <option value="None selected">Please select below</option>
                            <option value="Single">Single</option>
                            <option value="Married">Married</option>
                            <option value="widow">Widow</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Nationality</td>
                    <td>
                        <select name="Patientnationality">
                            <option value="">Please select below</option>
                            <option value="afghan">Afghan</option>
                            <option value="albanian">Albanian</option>
                            <option value="algerian">Algerian</option>
                            <option value="american">American</option>
                            <option value="andorran">Andorran</option>
                            <option value="angolan">Angolan</option>
                            <option value="antiguans">Antiguans</option>
                            <option value="argentinean">Argentinean</option>
                            <option value="armenian">Armenian</option>
                            <option value="australian">Australian</option>
                            <option value="austrian">Austrian</option>
                            <option value="azerbaijani">Azerbaijani</option>
                            <option value="bahamian">Bahamian</option>
                            <option value="bahraini">Bahraini</option>
                            <option value="bangladeshi">Bangladeshi</option>
                            <option value="barbadian">Barbadian</option>
                            <option value="barbudans">Barbudans</option>
                            <option value="batswana">Batswana</option>
                            <option value="belarusian">Belarusian</option>
                            <option value="belgian">Belgian</option>
                            <option value="belizean">Belizean</option>
                            <option value="beninese">Beninese</option>
                            <option value="bhutanese">Bhutanese</option>
                            <option value="bolivian">Bolivian</option>
                            <option value="bosnian">Bosnian</option>
                            <option value="brazilian">Brazilian</option>
                            <option value="british">British</option>
                            <option value="bruneian">Bruneian</option>
                            <option value="bulgarian">Bulgarian</option>
                            <option value="burkinabe">Burkinabe</option>
                            <option value="burmese">Burmese</option>
                            <option value="burundian">Burundian</option>
                            <option value="cambodian">Cambodian</option>
                            <option value="cameroonian">Cameroonian</option>
                            <option value="canadian">Canadian</option>
                            <option value="cape verdean">Cape Verdean</option>
                            <option value="central african">Central African</option>
                            <option value="chadian">Chadian</option>
                            <option value="chilean">Chilean</option>
                            <option value="chinese">Chinese</option>
                            <option value="colombian">Colombian</option>
                            <option value="comoran">Comoran</option>
                            <option value="congolese">Congolese</option>
                            <option value="costa rican">Costa Rican</option>
                            <option value="croatian">Croatian</option>
                            <option value="cuban">Cuban</option>
                            <option value="cypriot">Cypriot</option>
                            <option value="czech">Czech</option>
                            <option value="danish">Danish</option>
                            <option value="djibouti">Djibouti</option>
                            <option value="dominican">Dominican</option>
                            <option value="dutch">Dutch</option>
                            <option value="east timorese">East Timorese</option>
                            <option value="ecuadorean">Ecuadorean</option>
                            <option value="egyptian">Egyptian</option>
                            <option value="emirian">Emirian</option>
                            <option value="equatorial guinean">Equatorial Guinean</option>
                            <option value="eritrean">Eritrean</option>
                            <option value="estonian">Estonian</option>
                            <option value="ethiopian">Ethiopian</option>
                            <option value="fijian">Fijian</option>
                            <option value="filipino">Filipino</option>
                            <option value="finnish">Finnish</option>
                            <option value="french">French</option>
                            <option value="gabonese">Gabonese</option>
                            <option value="gambian">Gambian</option>
                            <option value="georgian">Georgian</option>
                            <option value="german">German</option>
                            <option value="ghanaian">Ghanaian</option>
                            <option value="greek">Greek</option>
                            <option value="grenadian">Grenadian</option>
                            <option value="guatemalan">Guatemalan</option>
                            <option value="guinea-bissauan">Guinea-Bissauan</option>
                            <option value="guinean">Guinean</option>
                            <option value="guyanese">Guyanese</option>
                            <option value="haitian">Haitian</option>
                            <option value="herzegovinian">Herzegovinian</option>
                            <option value="honduran">Honduran</option>
                            <option value="hungarian">Hungarian</option>
                            <option value="icelander">Icelander</option>
                            <option value="indian">Indian</option>
                            <option value="indonesian">Indonesian</option>
                            <option value="iranian">Iranian</option>
                            <option value="iraqi">Iraqi</option>
                            <option value="irish">Irish</option>
                            <option value="israeli">Israeli</option>
                            <option value="italian">Italian</option>
                            <option value="ivorian">Ivorian</option>
                            <option value="jamaican">Jamaican</option>
                            <option value="japanese">Japanese</option>
                            <option value="jordanian">Jordanian</option>
                            <option value="kazakhstani">Kazakhstani</option>
                            <option value="kenyan">Kenyan</option>
                            <option value="kittian and nevisian">Kittian and Nevisian</option>
                            <option value="kuwaiti">Kuwaiti</option>
                            <option value="kyrgyz">Kyrgyz</option>
                            <option value="laotian">Laotian</option>
                            <option value="latvian">Latvian</option>
                            <option value="lebanese">Lebanese</option>
                            <option value="liberian">Liberian</option>
                            <option value="libyan">Libyan</option>
                            <option value="liechtensteiner">Liechtensteiner</option>
                            <option value="lithuanian">Lithuanian</option>
                            <option value="luxembourger">Luxembourger</option>
                            <option value="macedonian">Macedonian</option>
                            <option value="malagasy">Malagasy</option>
                            <option value="malawian">Malawian</option>
                            <option value="malaysian">Malaysian</option>
                            <option value="maldivan">Maldivan</option>
                            <option value="malian">Malian</option>
                            <option value="maltese">Maltese</option>
                            <option value="marshallese">Marshallese</option>
                            <option value="mauritanian">Mauritanian</option>
                            <option value="mauritian">Mauritian</option>
                            <option value="mexican">Mexican</option>
                            <option value="micronesian">Micronesian</option>
                            <option value="moldovan">Moldovan</option>
                            <option value="monacan">Monacan</option>
                            <option value="mongolian">Mongolian</option>
                            <option value="moroccan">Moroccan</option>
                            <option value="mosotho">Mosotho</option>
                            <option value="motswana">Motswana</option>
                            <option value="mozambican">Mozambican</option>
                            <option value="namibian">Namibian</option>
                            <option value="nauruan">Nauruan</option>
                            <option value="nepalese">Nepalese</option>
                            <option value="new zealander">New Zealander</option>
                            <option value="ni-vanuatu">Ni-Vanuatu</option>
                            <option value="nicaraguan">Nicaraguan</option>
                            <option value="nigerien">Nigerien</option>
                            <option value="north korean">North Korean</option>
                            <option value="northern irish">Northern Irish</option>
                            <option value="norwegian">Norwegian</option>
                            <option value="omani">Omani</option>
                            <option value="pakistani">Pakistani</option>
                            <option value="palauan">Palauan</option>
                            <option value="panamanian">Panamanian</option>
                            <option value="papua new guinean">Papua New Guinean</option>
                            <option value="paraguayan">Paraguayan</option>
                            <option value="peruvian">Peruvian</option>
                            <option value="polish">Polish</option>
                            <option value="portuguese">Portuguese</option>
                            <option value="qatari">Qatari</option>
                            <option value="romanian">Romanian</option>
                            <option value="russian">Russian</option>
                            <option value="rwandan">Rwandan</option>
                            <option value="saint lucian">Saint Lucian</option>
                            <option value="salvadoran">Salvadoran</option>
                            <option value="samoan">Samoan</option>
                            <option value="san marinese">San Marinese</option>
                            <option value="sao tomean">Sao Tomean</option>
                            <option value="saudi">Saudi</option>
                            <option value="scottish">Scottish</option>
                            <option value="senegalese">Senegalese</option>
                            <option value="serbian">Serbian</option>
                            <option value="seychellois">Seychellois</option>
                            <option value="sierra leonean">Sierra Leonean</option>
                            <option value="singaporean">Singaporean</option>
                            <option value="slovakian">Slovakian</option>
                            <option value="slovenian">Slovenian</option>
                            <option value="solomon islander">Solomon Islander</option>
                            <option value="somali">Somali</option>
                            <option value="south african">South African</option>
                            <option value="south korean">South Korean</option>
                            <option value="spanish">Spanish</option>
                            <option value="sri lankan">Sri Lankan</option>
                            <option value="sudanese">Sudanese</option>
                            <option value="surinamer">Surinamer</option>
                            <option value="swazi">Swazi</option>
                            <option value="swedish">Swedish</option>
                            <option value="swiss">Swiss</option>
                            <option value="syrian">Syrian</option>
                            <option value="taiwanese">Taiwanese</option>
                            <option value="tajik">Tajik</option>
                            <option value="tanzanian">Tanzanian</option>
                            <option value="thai">Thai</option>
                            <option value="togolese">Togolese</option>
                            <option value="tongan">Tongan</option>
                            <option value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
                            <option value="tunisian">Tunisian</option>
                            <option value="turkish">Turkish</option>
                            <option value="tuvaluan">Tuvaluan</option>
                            <option value="ugandan">Ugandan</option>
                            <option value="ukrainian">Ukrainian</option>
                            <option value="uruguayan">Uruguayan</option>
                            <option value="uzbekistani">Uzbekistani</option>
                            <option value="venezuelan">Venezuelan</option>
                            <option value="vietnamese">Vietnamese</option>
                            <option value="welsh">Welsh</option>
                            <option value="yemenite">Yemenite</option>
                            <option value="zambian">Zambian</option>
                            <option value="zimbabwean">Zimbabwean</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Email *</td>
                    <td><input type="text" name="PatientEmailid" maxlength="100" /></td>
                </tr>
                <tr>
                    <td>Father's Name/Husband's Name</td>
                    <td><input type="text" name="Patientfathername" maxlength="100" /></td>
                </tr>
                <tr>
                    <td>Occupation</td>
                    <td><input type="text" name="Patientoccupation" maxlength="100" /></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><input type="text" name="Patientaddress" maxlength="100" /></td>
                </tr>
                
                                    <tr>
                        <td>Checkbox</td>
                                     <input type="checkbox" value="Maths" name="Checkbox"/>Maths
                                        <input type="checkbox" value="Physics" name="n1"/>Physics
                                        <input type="checkbox" value="english" name="n2"/>english
                                        <input type="checkbox" value="computer" name="n3"/>computer
            </tr>
            
                
                
                
                
                
                
                
                
                
                
                
                
                
                

                <!--                <tr>
                                    <td>User ID</td>
                                    <td><input type="text" name="userId" maxlength="100" /></td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td><input type="text" name="password" maxlength="100" /></td>
                                </tr>
                -->


<!--                <tr>

                    <td>Checkbox</td>

                <input type="checkbox" value="CardiacStent" name="Checkbox"/>CardiacStent

                <input type="checkbox" value="Maths" name="Checkbox"/>fever
                <input type="checkbox" value="english" name="Checkbox"/>Historyof
                <input type="checkbox" value="computer" name="Checkbox"/>Hndocarditis

                <input type="checkbox" value="Maths" name="checkbok"/>Maths
                <input type="checkbox" value="Physics" name="Checkbox"/>Physics
                <input type="checkbox" value="english" name="Checkbox"/>english
                <input type="checkbox" value="computer" name="Checkbox"/>computer

                                
                                <input type="checkbox" value="Artificialheartvalve" name="Artificialheartvalve"/>Artificialheartvalve
                                <input type="checkbox" value="RepairedHeartdefect" name="RepairedHeartdefect"/>RepairedHeartdefect
                                <input type="checkbox" value="Pacemakerimplantabledefibrillator" name="Pacemakerimplantabledefibrillator"/>Pacemakerimplantabledefibrillator
                                <input type="checkbox" value="Artificialprosthesisheartvalvejoints" name="Artificialprosthesisheartvalvejoints"/>Artificialprosthesisheartvalvejoints
                                <input type="checkbox" value="RheumaticFever" name="RheumaticFever"/>RheumaticFever
                </tr>          -->

                <tr>
                    <td colspan="2" align="center">

                     <input type="button" value="Next" onclick="window.location.href='MedicalHistory.jsp'"/>
                        <input type="submit" value="Submit">
                        <input type="reset" value="Reset">
                    </td>
                </tr>
            </table>

        </form>

    </body>
</html>
